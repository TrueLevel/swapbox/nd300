// Copyright 2022 TrueLevel SA
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
// SPDX-License-Identifier: MPL-2.0

package nd300

import (
	"errors"
	"fmt"
)

type Status struct {
	Msg   string
	Value byte
}

func (s Status) String() string {
	if s.Msg == "" {
		return "unknown"
	}

	return s.Msg
}

// nolint: gomnd // Numbers are from the ND-300KM manual.
var (
	PayoutSuccess   = Status{Value: 0xAA, Msg: "payout success"}       // Manual description: 'Payout Successful'.
	PayoutFailure   = Status{Value: 0xBB, Msg: "payout failure"}       // Manual description: 'Payout Fails'.
	StatusOk        = Status{Value: 0x00, Msg: "ok"}                   // Manual description: 'Status fine'.
	NoteEmpty       = Status{Value: 0x01, Msg: "note dispenser empty"} // Manual description: 'empty note'.
	NoteLow         = Status{Value: 0x02, Msg: "notes amount low"}     // Manual description: 'Stock less'.
	NoteJam         = Status{Value: 0x03, Msg: "note jam"}
	OverLength      = Status{Value: 0x04, Msg: "note over length"}
	NoteNotExit     = Status{Value: 0x05, Msg: "note not exited"}
	SensorError     = Status{Value: 0x06, Msg: "sensor error"}      // Manual description: 'Sensor error (Reserve)'.
	DoubleNoteError = Status{Value: 0x07, Msg: "double note error"} // Manual description: 'Double note error (Reserve)'.
	MotorError      = Status{Value: 0x08, Msg: "motor error"}
	DispensingBusy  = Status{Value: 0x09, Msg: "note dispenser busy"}
	SensorAdjusting = Status{Value: 0x0A, Msg: "sensor adjusting"} // Manual description: 'Sensor adjusting (Reserve)'.
	ChecksumError   = Status{Value: 0x0B, Msg: "checksum error"}
	LowPowerError   = Status{Value: 0x0C, Msg: "low power error"}

	ErrBadLen        = errors.New("bad message length")
	ErrBadSTX        = errors.New("bad STX prefix")
	ErrChecksum      = errors.New("invalid checksum")
	ErrUnknownStatus = errors.New("unknown status")
	ErrNotAStatus    = errors.New("not a status")
)

func (m Msg) ValidateAsStatus() error {
	if len(m) != MsgLen {
		return ErrBadLen
	}

	if m[idxStx] != STX {
		return ErrBadSTX
	}

	if m[idxMS] != StatusFlag {
		return ErrNotAStatus
	}

	if cksum := computeChecksum(m); cksum != m[idxCksum] {
		return fmt.Errorf("%w: expected %x, go %x", ErrChecksum, cksum, m[idxCksum])
	}

	switch m[idxCmd] {
	case PayoutSuccess.Value,
		PayoutFailure.Value,
		StatusOk.Value,
		NoteEmpty.Value,
		NoteLow.Value,
		NoteJam.Value,
		OverLength.Value,
		NoteNotExit.Value,
		SensorError.Value,
		DoubleNoteError.Value,
		MotorError.Value,
		DispensingBusy.Value,
		SensorAdjusting.Value,
		ChecksumError.Value,
		LowPowerError.Value:
		return nil
	default:
		return ErrUnknownStatus
	}
}

func (m Msg) Status() Status {
	var status Status

	if m[idxMS] != StatusFlag {
		return Status{
			Msg:   "not a status",
			Value: m[idxStatus],
		}
	}

	switch m[idxStatus] {
	case PayoutSuccess.Value:
		status = PayoutSuccess
	case PayoutFailure.Value:
		status = PayoutFailure
	case StatusOk.Value:
		status = StatusOk
	case NoteEmpty.Value:
		status = NoteEmpty
	case NoteLow.Value:
		status = NoteLow
	case NoteJam.Value:
		status = NoteJam
	case OverLength.Value:
		status = OverLength
	case NoteNotExit.Value:
		status = NoteNotExit
	case SensorError.Value:
		status = SensorError
	case DoubleNoteError.Value:
		status = DoubleNoteError
	case MotorError.Value:
		status = MotorError
	case DispensingBusy.Value:
		status = DispensingBusy
	case SensorAdjusting.Value:
		status = SensorAdjusting
	case ChecksumError.Value:
		status = ChecksumError
	case LowPowerError.Value:
		status = LowPowerError
	default:
		status = Status{
			Value: m[idxStatus],
			Msg:   "unknown status",
		}
	}

	return status
}
